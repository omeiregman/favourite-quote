const express = require("express");
const http = require("http");
const axios = require("axios");
const app = express();
const port = 5000;

app.get("/", (req, res) => res.send({ message: "ping" }));
setInterval(() => {
  writeData();
}, 5000);
app.listen(port, () => console.log("App listening on port ", port));

count = 0;

const writeData = () => {
  console.log("Write count: ", count);
  count++;
  const options = {
    host: "web",
    port: 8000,
    path: "/",
    method: "GET",
  };
  const req = http.request(options, function (res) {
    res.setEncoding("utf8");
    res.on("data", function (chunk) {
      console.log("BODY: " + chunk);
    });
  });

  req.on("error", function (e) {
    console.log("problem with request: " + e.message);
  });

  req.write("data\n");
  req.write("data\n");
  req.end();
};
